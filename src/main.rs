mod robdd;
use robdd::parser::parse;
use robdd::parser::{GrammerItem, ParseNode};
use robdd::{ApplyOperator, Robdd};

use std::io::{self, Write};

fn main() {
    // "global" variables
    let mut robdd = Robdd::new(ParseNode::new(GrammerItem::Literal(true)), 0);
    let available_commands = vec![
        "load", "infix", "build", "display", "quit", "apply", "restrict", "satcount", "anysat",
    ];
    println!(
        "Enter a command:\nAvailable commands: {:?}",
        available_commands
    );
    loop {
        // get command
        print!("$ ");
        io::stdout().flush().unwrap();
        let mut command = String::new();
        if let Ok(_) = io::stdin().read_line(&mut command) {
            let command_str = command.trim();
            // handle command
            let res: Result<bool, String> = match command_str {
                "load" => {
                    println!("Enter in an expression:");
                    let mut expression = String::new();
                    let result = if let Ok(_) = io::stdin().read_line(&mut expression) {
                        parse::parse(&expression)
                    } else {
                        Err(format!("Unable to read expression"))
                    };
                    match result {
                        Ok((t, n)) => {
                            robdd = Robdd::new(t, n);
                            Ok(true)
                        }
                        Err(e) => Err(e),
                    }
                }
                "infix" => {
                    println!("{}", parse::to_infix(&robdd.expr));
                    Ok(true)
                }
                "build" => match robdd.build() {
                    Ok(root) => {
                        println!("Built with new root at {}", root);
                        Ok(true)
                    }
                    Err(e) => Err(e),
                },
                "display" => {
                    println!("{:#?}", robdd.t);
                    Ok(true)
                }
                "quit" => {
                    break;
                }
                "apply" => {
                    // need to get a second robdd
                    let mut second_expr = String::new();
                    println!("Enter in a second expression:");
                    let result = if let Ok(_) = io::stdin().read_line(&mut second_expr) {
                        parse::parse(&second_expr)
                    } else {
                        Err(format!("Unable to read expression"))
                    };
                    match result {
                        Ok((t, n)) => {
                            let mut second_robdd = Robdd::new(t, n);
                            match second_robdd.build() {
                                Ok(_) => {
                                    match get_apply_operator() {
                                        Ok(op) => {
                                            match robdd.apply(op, &second_robdd) {
                                                Ok(new_root) => {
                                                    println!("New root: {}", new_root);
                                                    Ok(true)
                                                },
                                                Err(e) => Err(e)
                                            }
                                        },
                                        Err(e) => Err(e)
                                    }
                                }
                                Err(e) => Err(e),
                            }
                        }
                        Err(e) => Err(e),
                    }
                },
                "restrict" => {
                    // get variable and value
                    let mut var_str = String::new();
                    println!("Enter in a second expression:");
                    let varnum = if let Ok(_) = io::stdin().read_line(&mut var_str) {
                        match var_str.trim().parse() {
                            Ok(n) => Ok(n),
                            Err(_) => Err(format!("Unable to parse number"))
                        }
                    } else {
                        Err(format!("Unable to read expression"))
                    };
                    let mut second_expr = String::new();
                    println!("Enter in a value:");
                    let value = if let Ok(_) = io::stdin().read_line(&mut second_expr) {
                        let temp: Result<u32, String> = match var_str.trim().parse() {
                            Ok(n) => Ok(n),
                            Err(_) => Err(format!("Unable to parse number"))
                        };
                        temp
                    } else {
                        Err(format!("Unable to read expression"))
                    };
                    match (varnum, value) {
                        (Ok(j), Ok(b)) => {
                            match robdd.restrict(j, b == 1) {
                                Ok(new_root) => {
                                    println!("New root: {}", new_root);
                                    Ok(true)
                                },
                                Err(e) => Err(e)
                            }
                        },
                        _ => Err(format!("Error getting value(s)"))
                    }
                },
                "satcount" => {
                    println!("{}", robdd.sat_count());
                    Ok(true)
                },
                "anysat" => {
                    match robdd.any_sat() {
                        Ok(assign) => {
                            println!("Assignment (None = Don't care): {:?}", assign);
                            Ok(true)
                        },
                        Err(e) => Err(e)
                    }
                }
                _ => Err(format!("Invalid command {}", command)),
            };
            if let Err(e) = res {
                println!("Error: {}", e);
            }
        } else {
            println!("Could not read command");
        }
    }
}

fn get_apply_operator() -> Result<ApplyOperator, String> {
    // as for operator
    let mut operator = String::new();
    println!("Enter in an operator (and, or, nand, nor, add, equiv, imp):");
    let result = if let Ok(_) = io::stdin().read_line(&mut operator) {
        let op_str = operator.trim();
        match op_str {
            "and" => Ok(ApplyOperator::And),
            "nand" => Ok(ApplyOperator::Nand),
            "or" => Ok(ApplyOperator::Or),
            "nor" => Ok(ApplyOperator::Nor),
            "add" => Ok(ApplyOperator::Add),
            "equiv" => Ok(ApplyOperator::Equiv),
            "imp" => Ok(ApplyOperator::Imp),
            _ => Err(format!("Unknown op {}", operator))
        }
    } else {
        Err(format!("Unable to read expression"))
    };
    result
}
