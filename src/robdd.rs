pub mod parser;

pub mod ops;

// definitions
use parser::ParseNode;
use std::cmp::Ordering;
use std::collections::{HashMap};

#[derive(PartialEq, Eq, Hash, Clone, Copy, Debug)]
pub enum ApplyOperator {
    And,
    Nand,
    Nor,
    Or,
    Imp,
    Equiv,
    Add,
}

#[derive(PartialEq, Eq, Hash, Clone, Copy, Debug)]
pub struct RobddEntry {
    var: u32,
    low: u32,
    high: u32,
}

impl RobddEntry {
    fn new(i: u32, l: u32, h: u32) -> RobddEntry {
        RobddEntry {
            var: i,
            low: l,
            high: h,
        }
    }
}

pub struct Robdd {
    // Table
    pub t: Vec<RobddEntry>,
    pub h: HashMap<RobddEntry, u32>,
    pub num_vars: u32,
    pub expr: ParseNode,
    pub root: u32,
}

impl Robdd {
    pub fn new(t: ParseNode, n: u32) -> Robdd {
        let mut temp = Vec::new();
        let entry = RobddEntry::new(n + 1, 0, 0);
        temp.push(entry);
        temp.push(entry);
        Robdd {
            t: temp,
            h: HashMap::new(),
            num_vars: n,
            expr: t,
            root: 0,
        }
    }

    fn add_new_node(&mut self, i: u32, l: u32, h: u32) -> u32 {
        let entry = RobddEntry::new(i, l, h);
        self.t.push(entry);
        if i > self.num_vars {
            self.num_vars = i;
            self.t[0].var = self.num_vars + 1;
            self.t[1].var = self.num_vars + 1;
        }
        (self.t.len() - 1) as u32
    }

    fn lookup(&self, i: u32, l: u32, h: u32) -> Option<&u32> {
        let entry = RobddEntry::new(i, l, h);
        self.h.get(&entry)
    }

    fn insert(&mut self, i: u32, l: u32, h: u32, u: u32) {
        let entry = RobddEntry::new(i, l, h);
        self.h.insert(entry, u);
    }

    pub fn mk(&mut self, i: u32, l: u32, h: u32) -> u32 {
        // equal case
        if l == h {
            l
        } else {
            // match on if the node already exists in the table
            // if it does then return it,
            // if it does not then create it
            match self.lookup(i, l, h) {
                Some(&u) => u,
                None => {
                    let u = self.add_new_node(i, l, h);
                    self.insert(i, l, h, u);
                    u
                }
            }
        }
    }

    fn build_expression(&mut self, values: &mut Vec<Option<bool>>, i: u32) -> Result<u32, String> {
        // print T every iter
        // println!("I: {}", i);
        // println!("Values: {:?}", values);
        // println!("T:\n{:#?}", self.t);
        if i > self.num_vars {
            // println!("Eval Called");
            match ops::evaluate_expression(&self.expr, values) {
                Ok(res) => {
                    if res {
                        Ok(1)
                    } else {
                        Ok(0)
                    }
                }
                Err(e) => Err(e),
            }
        } else {
            // set xi = 0
            values.push(Some(false));
            let v0 = self.build_expression(values, i + 1)?;
            values.pop();
            // set xi = 1
            values.push(Some(true));
            let v1 = self.build_expression(values, i + 1)?;
            values.pop();
            Ok(self.mk(i, v0, v1))
        }
    }

    pub fn build(&mut self) -> Result<u32, String> {
        let mut values = vec![None];
        match self.build_expression(&mut values, 1) {
            Ok(root) => {
                self.root = root;
                Ok(self.root)
            }
            Err(v) => Err(v),
        }
    }

    fn apply_operator_non_recursive(op: ApplyOperator, u1: u32, u2: u32) -> u32 {
        let u1_b = u1 == 1;
        let u2_b = u2 == 1;
        let res = match op {
            ApplyOperator::And => u1_b && u2_b,
            ApplyOperator::Nand => !(u1_b && u2_b),
            ApplyOperator::Or => u1_b || u2_b,
            ApplyOperator::Nor => !(u1_b || u2_b),
            ApplyOperator::Imp => !((u1_b == true) && (u2_b == false)),
            ApplyOperator::Equiv => u1_b == u2_b,
            ApplyOperator::Add => u1_b ^ u2_b,
        };
        res as u32
    }

    fn apply_operator(
        &mut self,
        u1: u32,
        u2: u32,
        op: ApplyOperator,
        g: &mut HashMap<(u32, u32), u32>,
    ) -> Result<u32, String> {
        if let Some(&value) = g.get(&(u1, u2)) {
            Ok(value)
        } else {
            let new_u = if u2 < 2 && u1 < 2 {
                Robdd::apply_operator_non_recursive(op, u1, u2)
            } else {
                let var_u1 = self.t[u1 as usize].var;
                let var_u2 = self.t[u2 as usize].var;
                let low_u1 = self.t[u1 as usize].low;
                let low_u2 = self.t[u2 as usize].low;
                let high_u1 = self.t[u1 as usize].high;
                let high_u2 = self.t[u2 as usize].high;
                match var_u1.cmp(&var_u2) {
                    Ordering::Equal => {
                        let l = self.apply_operator(low_u1, low_u2, op, g)?;
                        let h = self.apply_operator(high_u1, high_u2, op, g)?;
                        self.mk(var_u1, l, h)
                    }
                    Ordering::Less => {
                        let l = self.apply_operator(low_u1, u2, op, g)?;
                        let h = self.apply_operator(high_u1, u2, op, g)?;
                        self.mk(var_u1, l, h)
                    }
                    Ordering::Greater => {
                        let l = self.apply_operator(u1, low_u2, op, g)?;
                        let h = self.apply_operator(u1, high_u2, op, g)?;
                        self.mk(var_u2, l, h)
                    }
                }
            };
            // add to g
            g.insert((u1, u2), new_u);
            Ok(new_u)
        }
    }

    fn merge_entry(&mut self, u: u32, entries: &Vec<RobddEntry>) -> u32 {
        let entry = entries[u as usize];
        if u < 2 {
            u
        } else {
            let new_low = self.merge_entry(entry.low, entries);
            let new_high = self.merge_entry(entry.high, entries);
            self.mk(entry.var, new_low, new_high)
        }
    }

    fn merge_with(&mut self, other: &Robdd) -> u32 {
        self.merge_entry(other.root, &other.t)
    }

    pub fn apply(&mut self, op: ApplyOperator, other: &Robdd) -> Result<u32, String> {
        // merge other into self
        let other_root = self.merge_with(other);
        let mut g = HashMap::new();
        self.apply_operator(self.root, other_root, op, &mut g)
    }

    fn res(&mut self, u: u32, j: u32, b: bool) -> Result<u32, String> {
        let var = self.t[u as usize].var;
        let low = self.t[u as usize].low;
        let high = self.t[u as usize].high;
        match var.cmp(&j) {
            Ordering::Greater => Ok(u),
            Ordering::Less => {
                let new_low = self.res(low, j, b)?;
                let new_high = self.res(high, j, b)?;
                Ok(self.mk(var, new_low, new_high))
            }
            Ordering::Equal => {
                if b {
                    self.res(high, j, b)
                } else {
                    self.res(low, j, b)
                }
            }
        }
    }

    // fn find_used(&self, u: u32, used: &mut HashSet<u32>) {
    //     used.insert(u);
    //     if u >= 2 {
    //         let low = self.t[u as usize].low;
    //         let high = self.t[u as usize].high;
    //         self.find_used(low, used);
    //         self.find_used(high, used);
    //     }
    // }

    // fn remove_unused(&mut self) {
    //     let mut used = HashSet::new();
    //     self.find_used(self.root, used);

    // }

    pub fn restrict(&mut self, j: u32, b: bool) -> Result<u32, String> {
        self.res(self.root, j, b)
    }

    fn count(&self, u: u32, results: &mut HashMap<u32, u32>) -> u32 {
        match results.get(&u) {
            Some(&res) => res,
            None => {
                let res = if u < 2 {
                    u
                } else {
                    let var = self.t[u as usize].var;
                    let low = self.t[u as usize].low;
                    let high = self.t[u as usize].high;
                    let var_low = self.t[low as usize].var;
                    let var_high = self.t[high as usize].var;
                    let lhs = (2 as u32).pow(var_low - var - 1) * self.count(low, results);
                    let rhs = (2 as u32).pow(var_high - var - 1) * self.count(high, results);
                    lhs + rhs
                };
                results.insert(u, res);
                res
            }
        }
    }

    pub fn sat_count(&self) -> u32 {
        let mut results = HashMap::new();
        let var = self.t[self.root as usize].var;
        (2 as u32).pow(var - 1) * self.count(self.root, &mut results)
    }

    fn find_sat_assignment(
        &self,
        u: u32,
        assignments: &mut Vec<Option<bool>>,
    ) -> Result<bool, String> {
        if u == 0 {
            Err(format!("Unable to find assignment"))
        } else if u == 1 {
            Ok(true)
        } else {
            let var = self.t[u as usize].var;
            let low = self.t[u as usize].low;
            let high = self.t[u as usize].high;
            if low == 0 {
                assignments[var as usize] = Some(true);
                self.find_sat_assignment(high, assignments)
            } else {
                assignments[var as usize] = Some(false);
                self.find_sat_assignment(low, assignments)
            }
        }
    }

    pub fn any_sat(&self) -> Result<Vec<Option<bool>>, String> {
        let mut assignments = vec![None; (self.num_vars + 1) as usize];
        self.find_sat_assignment(self.root, &mut assignments)?;
        Ok(assignments)
    }
}

#[cfg(test)]
mod tests {
    use super::ops;
    use super::ApplyOperator;
    use crate::robdd::parser::parse;
    use crate::robdd::parser::{GrammerItem, ParseNode};
    use crate::robdd::{Robdd, RobddEntry};
    use std::collections::HashMap;
    #[test]
    fn mk_basic() {
        // init the table
        // vector
        let mut v = Vec::new();
        let e = RobddEntry::new(0, 1, 2);
        let p = RobddEntry::new(1, 2, 3);
        v.push(e); // copy in
        v.push(p); // copy in
                   // init inverted index
        let mut h = HashMap::new();
        h.insert(e, 0);
        h.insert(p, 1);
        let mut robdd = Robdd {
            t: v,
            h: h,
            num_vars: 0,
            expr: ParseNode::new(GrammerItem::Not),
            root: 0,
        };
        // l == h
        assert_eq!(robdd.mk(0, 4, 4), 4);
        // in the table already
        assert_eq!(robdd.mk(0, 1, 2), 0);
        assert_eq!(robdd.mk(1, 2, 3), 1);
        // not in the table
        assert_eq!(robdd.mk(2, 2, 3), 2);
        // in the table already
        assert_eq!(robdd.mk(2, 2, 3), 2);
        // not in the table
        assert_eq!(robdd.mk(3, 2, 3), 3);
        // in the table already
        assert_eq!(robdd.mk(3, 2, 3), 3);
    }

    #[test]
    fn build_contraditions() {
        let test = "not(1)";
        let robdd1 = parse::parse(test).and_then(|(t, n)| {
            let mut robdd = Robdd::new(t, n);
            let _res = robdd.build();
            Ok(robdd)
        });
        let test = "and(0, x1)";
        let robdd2 = parse::parse(test).and_then(|(t, n)| {
            let mut robdd = Robdd::new(t, n);
            let _res = robdd.build();
            Ok(robdd)
        });
        let test = "imp(1, 0)";
        let robdd3 = parse::parse(test).and_then(|(t, n)| {
            let mut robdd = Robdd::new(t, n);
            let _res = robdd.build();
            Ok(robdd)
        });
        let test = "equiv(1, 0)";
        let robdd4 = parse::parse(test).and_then(|(t, n)| {
            let mut robdd = Robdd::new(t, n);
            let _res = robdd.build();
            Ok(robdd)
        });
        let test = "and(not(x1), x1)";
        let robdd5 = parse::parse(test).and_then(|(t, n)| {
            let mut robdd = Robdd::new(t, n);
            let _res = robdd.build();
            Ok(robdd)
        });
        assert!(2 == robdd1.unwrap().t.len());
        assert!(2 == robdd2.unwrap().t.len());
        assert!(2 == robdd3.unwrap().t.len());
        assert!(2 == robdd4.unwrap().t.len());
        assert!(2 == robdd5.unwrap().t.len());
    }

    #[test]
    fn build_taut() {
        let test = "not(0)";
        let robdd1 = parse::parse(test).and_then(|(t, n)| {
            let mut robdd = Robdd::new(t, n);
            let res = robdd.build();
            Ok((robdd, res))
        });
        let test = "or(1, x1)";
        let robdd2 = parse::parse(test).and_then(|(t, n)| {
            let mut robdd = Robdd::new(t, n);
            let res = robdd.build();
            Ok((robdd, res))
        });
        let test = "imp(0, x1)";
        let robdd3 = parse::parse(test).and_then(|(t, n)| {
            let mut robdd = Robdd::new(t, n);
            let res = robdd.build();
            Ok((robdd, res))
        });
        let test = "equiv(x1, x1)";
        let robdd4 = parse::parse(test).and_then(|(t, n)| {
            let mut robdd = Robdd::new(t, n);
            let res = robdd.build();
            Ok((robdd, res))
        });
        let test = "equiv(imp(and(x1,x2),x3),imp(x1,imp(x2,x3)))";
        let robdd5 = parse::parse(test).and_then(|(t, n)| {
            let mut robdd = Robdd::new(t, n);
            let res = robdd.build();
            Ok((robdd, res))
        });
        let test = "or(or(and(or(x1,x2),imp(x1,x3)),not(or(x1,x2))),not(imp(x1,x3)))";
        let robdd6 = parse::parse(test).and_then(|(t, n)| {
            let mut robdd = Robdd::new(t, n);
            let res = robdd.build();
            Ok((robdd, res))
        });
        let (robdd, root) = robdd1.unwrap();
        assert!(2 == robdd.t.len() && Ok(1) == root);
        let (robdd, root) = robdd2.unwrap();
        assert!(2 == robdd.t.len() && Ok(1) == root);
        let (robdd, root) = robdd3.unwrap();
        assert!(2 == robdd.t.len() && Ok(1) == root);
        let (robdd, root) = robdd4.unwrap();
        assert!(2 == robdd.t.len() && Ok(1) == root);
        let (robdd, root) = robdd5.unwrap();
        assert!(2 == robdd.t.len() && Ok(1) == root);
        let (robdd, root) = robdd6.unwrap();
        assert!(2 == robdd.t.len() && Ok(1) == root);
    }

    #[test]
    fn apply_taut() {
        // apply equiv to two different tautologies
        let test1 = "equiv(x1, x1)";
        let test2 = "or(or(and(or(x1,x2),imp(x1,x3)),not(or(x1,x2))),not(imp(x1,x3)))";
        let (t1p, t1n) = parse::parse(test1).expect("Unable to parse");
        let (t2p, t2n) = parse::parse(test2).expect("Unable to parse");
        // make robdds
        let mut test1_robdd = Robdd::new(t1p, t1n);
        let mut test2_robdd = Robdd::new(t2p, t2n);
        // build each
        test1_robdd.build().expect("Unable to build");
        test2_robdd.build().expect("Unable to build");
        let new_root = test1_robdd
            .apply(ApplyOperator::Equiv, &test2_robdd)
            .expect("Unable to apply");
        assert!(2 == test1_robdd.t.len() && new_root == 1);
    }

    #[test]
    fn apply_contradictions() {
        // apply equiv to two different tautologies
        let test1 = "equiv(1, 0)";
        let test2 = "and(not(x1), x1)";
        let (t1p, t1n) = parse::parse(test1).expect("Unable to parse");
        let (t2p, t2n) = parse::parse(test2).expect("Unable to parse");
        // make robdds
        let mut test1_robdd = Robdd::new(t1p, t1n);
        let mut test2_robdd = Robdd::new(t2p, t2n);
        // build each
        test1_robdd.build().expect("Unable to build");
        test2_robdd.build().expect("Unable to build");
        let new_root = test1_robdd
            .apply(ApplyOperator::Equiv, &test2_robdd)
            .expect("Unable to apply");
        assert!(2 == test1_robdd.t.len() && new_root == 1);
    }

    #[test]
    fn apply_contr_taut() {
        // apply equiv to two different tautologies
        let test1 = "equiv(1, 0)";
        let test2 = "or(or(and(or(x1,x2),imp(x1,x3)),not(or(x1,x2))),not(imp(x1,x3)))";
        let (t1p, t1n) = parse::parse(test1).expect("Unable to parse");
        let (t2p, t2n) = parse::parse(test2).expect("Unable to parse");
        // make robdds
        let mut test1_robdd = Robdd::new(t1p, t1n);
        let mut test2_robdd = Robdd::new(t2p, t2n);
        // build each
        test1_robdd.build().expect("Unable to build");
        test2_robdd.build().expect("Unable to build");
        let new_root = test1_robdd
            .apply(ApplyOperator::Equiv, &test2_robdd)
            .expect("Unable to apply");
        assert!(2 == test1_robdd.t.len() && new_root == 0);
    }

    #[test]
    fn apply_add() {
        let test1 = "and(and(and(x1, not(x2)), x3), x4)";
        let test2 = "and(and(and(not(x1), x2), x3), x4)";
        let (t1p, t1n) = parse::parse(test1).expect("Unable to parse");
        let (t2p, t2n) = parse::parse(test2).expect("Unable to parse");
        // make robdds
        let mut test1_robdd = Robdd::new(t1p, t1n);
        let mut test2_robdd = Robdd::new(t2p, t2n);
        // build each
        test1_robdd.build().expect("Unable to build");
        test2_robdd.build().expect("Unable to build");
        let _new_root = test1_robdd
            .apply(ApplyOperator::Add, &test2_robdd)
            .expect("Unable to apply");

        let compare = vec![
            RobddEntry::new(5, 0, 0),
            RobddEntry::new(5, 0, 0),
            RobddEntry::new(4, 0, 1),
            RobddEntry::new(3, 0, 2),
            RobddEntry::new(2, 3, 0),
            RobddEntry::new(1, 0, 4),
            RobddEntry::new(2, 0, 3),
            RobddEntry::new(1, 6, 0),
            RobddEntry::new(1, 6, 4),
        ];
        assert_eq!(compare, test1_robdd.t);
    }

    #[test]
    fn build_equiv() {
        let test1 = "and(and(and(x1, x2), x3), not(x4))";
        let test2 = "and(and(and(x1, and(or(x2, equiv(0, 1)), x2)), and(x3,x3)), not(x4))";
        let (t1p, t1n) = parse::parse(test1).expect("Unable to parse");
        let (t2p, t2n) = parse::parse(test2).expect("Unable to parse");
        // make robdds
        let mut test1_robdd = Robdd::new(t1p, t1n);
        let mut test2_robdd = Robdd::new(t2p, t2n);
        // build each
        test1_robdd.build().expect("Unable to build");
        test2_robdd.build().expect("Unable to build");
        assert_eq!(test1_robdd.t, test2_robdd.t);
    }

    #[test]
    fn restrict_basic() {
        let test = "or(equiv(x1, x2), x3)";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        test1_robdd.restrict(2, false).expect("Unable to restrict");

        let compare = vec![
            RobddEntry::new(4, 0, 0),
            RobddEntry::new(4, 0, 0),
            RobddEntry::new(3, 0, 1),
            RobddEntry::new(2, 1, 2),
            RobddEntry::new(2, 2, 1),
            RobddEntry::new(1, 3, 4),
            RobddEntry::new(1, 1, 2),
        ];
        assert_eq!(compare, test1_robdd.t);
    }

    #[test]
    fn sat_count_basic() {
        let test = "equiv(x1, not(imp(equiv(imp(imp(and(or(x1, x2), or(x3, x4)), not(x1)), equiv(not(x5), imp(equiv(x3, x6), imp(x7, x8)))), not(or(x9, not(imp(x9, x10))))), imp(not(not(and(equiv(x7, x10), imp(x10, x10)))), or(or(x1, or(not(x1), or(x10, x4))), equiv(or(or(x10, x10), equiv(x10, x10)), and(x10, equiv(x10, x10))))))))";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        let count = test1_robdd.sat_count();
        assert_eq!(512, count);
        let test = "or(imp(not(not(and(imp(imp(imp(x1, x1), and(x1, x2)), imp(and(x3, x4), and(x5, x6))), imp(imp(equiv(x7, x8), x1), and(and(x5, x9), equiv(x10, x2)))))), imp(equiv(and(imp(not(and(x1, x10)), and(and(x2, x10), not(x10))), equiv(and(imp(x10, x10), not(x10)), x10)), and(imp(equiv(not(x10), x10), and(imp(x10, x4), x10)), x10)), equiv(or(x10, and(not(imp(x10, x3)), x10)), equiv(and(and(imp(x9, x10), not(x10)), not(not(x5))), not(or(equiv(x10, x10), and(x2, x1))))))), equiv(imp(not(equiv(and(not(equiv(x10, x10)), x10), equiv(equiv(and(x10, x10), x10), imp(or(x10, x10), not(x10))))), and(not(or(imp(imp(x1, x10), not(x5)), and(equiv(x9, x5), and(x10, x7)))), not(not(not(not(x4)))))), x10))";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        let count = test1_robdd.sat_count();
        assert_eq!(1000, count);
    }

    #[test]
    fn sat_count_contradiction() {
        let test = "and(x1, not(x1))";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        let count = test1_robdd.sat_count();
        assert_eq!(0, count);
        let test = "equiv(1, 0)";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        let count = test1_robdd.sat_count();
        assert_eq!(0, count);
        let test = "imp(1, 0)";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        let count = test1_robdd.sat_count();
        assert_eq!(0, count);
        let test = "and(0, x1)";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        let count = test1_robdd.sat_count();
        assert_eq!(0, count);
        let test = "not(1)";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        let count = test1_robdd.sat_count();
        assert_eq!(0, count);
    }

    #[test]
    fn sat_count_tautology() {
        let test = "equiv(x1, x1)";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        let count = test1_robdd.sat_count();
        let max_count = (2 as u32).pow(test1_robdd.num_vars);
        assert_eq!(max_count, count);
        let test = "or(1, x1)";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        let count = test1_robdd.sat_count();
        let max_count = (2 as u32).pow(test1_robdd.num_vars);
        assert_eq!(max_count, count);
        let test = "imp(0, x1)";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        let count = test1_robdd.sat_count();
        let max_count = (2 as u32).pow(test1_robdd.num_vars);
        assert_eq!(max_count, count);
        let test = "equiv(imp(and(x1,x2),x3),imp(x1,imp(x2,x3)))";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        let count = test1_robdd.sat_count();
        let max_count = (2 as u32).pow(test1_robdd.num_vars);
        assert_eq!(max_count, count);
        let test = "or(or(and(or(x1,x2),imp(x1,x3)),not(or(x1,x2))),not(imp(x1,x3)))";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        let count = test1_robdd.sat_count();
        let max_count = (2 as u32).pow(test1_robdd.num_vars);
        assert_eq!(max_count, count);
        let test = "not(0)";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        let count = test1_robdd.sat_count();
        let max_count = (2 as u32).pow(test1_robdd.num_vars);
        assert_eq!(max_count, count);
    }

    #[test]
    fn any_sat_basic() {
        let test = "and(not(equiv(x2, x3)), or(x3, imp(x1, x2)))";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        let mut assignment = test1_robdd.any_sat().expect("Unable to find assignment");
        ops::replace_none_dont_care(&mut assignment, true);
        // try eval
        let result =
            ops::evaluate_expression(&test1_robdd.expr, &assignment).expect("Could not evaluate");
        assert_eq!(true, result);
        let test = "or(x1, and(equiv(imp(or(x2, 0), and(x3, x4)), x5), imp(imp(not(x6), 1), imp(and(x2, x2), not(1)))))";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        let mut assignment = test1_robdd.any_sat().expect("Unable to find assignment");
        ops::replace_none_dont_care(&mut assignment, true);
        // try eval
        let result =
            ops::evaluate_expression(&test1_robdd.expr, &assignment).expect("Could not evaluate");
        assert_eq!(true, result);
        let test = "or(0, x1)";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        let mut assignment = test1_robdd.any_sat().expect("Unable to find assignment");
        ops::replace_none_dont_care(&mut assignment, true);
        // try eval
        let result =
            ops::evaluate_expression(&test1_robdd.expr, &assignment).expect("Could not evaluate");
        assert_eq!(true, result);
        let test = "or(x2, equiv(and(x3, 0), not(x1)))";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        let mut assignment = test1_robdd.any_sat().expect("Unable to find assignment");
        ops::replace_none_dont_care(&mut assignment, true);
        // try eval
        let result =
            ops::evaluate_expression(&test1_robdd.expr, &assignment).expect("Could not evaluate");
        assert_eq!(true, result);
        let test = 
            "imp(not(equiv(and(equiv(x1, x2), imp(0, x3)), x1)), or(or(not(and(x4, x5)), x1), x6))";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        let mut assignment = test1_robdd.any_sat().expect("Unable to find assignment");
        ops::replace_none_dont_care(&mut assignment, true);
        // try eval
        let result =
            ops::evaluate_expression(&test1_robdd.expr, &assignment).expect("Could not evaluate");
        assert_eq!(true, result);
        let test = "imp(equiv(or(and(equiv(0, 1), 1), 1), not(or(and(x2, x2), x2))), and(1, x1))";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        let mut assignment = test1_robdd.any_sat().expect("Unable to find assignment");
        ops::replace_none_dont_care(&mut assignment, true);
        // try eval
        let result =
            ops::evaluate_expression(&test1_robdd.expr, &assignment).expect("Could not evaluate");
        assert_eq!(true, result);
        let test = "or(equiv(x1, x2), x3)";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        let mut assignment = test1_robdd.any_sat().expect("Unable to find assignment");
        ops::replace_none_dont_care(&mut assignment, true);
        // try eval
        let result =
            ops::evaluate_expression(&test1_robdd.expr, &assignment).expect("Could not evaluate");
        assert_eq!(true, result);
    }

    #[test]
    fn any_sat_contradiction() {
        let test = "and(x1, not(x1))";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        if let Ok(_) = test1_robdd.any_sat() {
            panic!("Expected no assignment");
        }
        let test = "equiv(1, 0)";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        if let Ok(_) = test1_robdd.any_sat() {
            panic!("Expected no assignment");
        }
        let test = "imp(1, 0)";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        if let Ok(_) = test1_robdd.any_sat() {
            panic!("Expected no assignment");
        }
        let test = "and(0, x1)";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        if let Ok(_) = test1_robdd.any_sat() {
            panic!("Expected no assignment");
        }
        let test = "not(1)";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        if let Ok(_) = test1_robdd.any_sat() {
            panic!("Expected no assignment");
        }
    }

    #[test]
    fn any_sat_tautology() {
        let test = "equiv(x1, x1)";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        let assignment = test1_robdd.any_sat()
            .expect("Unable to find assignment");
        for value in assignment {
            if let Some(_) = value {
                panic!("Expected all None");
            }
        }
        let test = "or(1, x1)";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        let assignment = test1_robdd.any_sat()
            .expect("Unable to find assignment");
        for value in assignment {
            if let Some(_) = value {
                panic!("Expected all None");
            }
        }
        let test = "imp(0, x1)";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        let assignment = test1_robdd.any_sat()
            .expect("Unable to find assignment");
        for value in assignment {
            if let Some(_) = value {
                panic!("Expected all None");
            }
        }
        let test = "equiv(imp(and(x1,x2),x3),imp(x1,imp(x2,x3)))";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        let assignment = test1_robdd.any_sat()
            .expect("Unable to find assignment");
        for value in assignment {
            if let Some(_) = value {
                panic!("Expected all None");
            }
        }
        let test = "or(or(and(or(x1,x2),imp(x1,x3)),not(or(x1,x2))),not(imp(x1,x3)))";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        let assignment = test1_robdd.any_sat()
            .expect("Unable to find assignment");
        for value in assignment {
            if let Some(_) = value {
                panic!("Expected all None");
            }
        }
        let test = "not(0)";
        let (t1p, t1n) = parse::parse(test).expect("Unable to parse");
        let mut test1_robdd = Robdd::new(t1p, t1n);
        test1_robdd.build().expect("Unable to build");
        let assignment = test1_robdd.any_sat()
            .expect("Unable to find assignment");
        for value in assignment {
            if let Some(_) = value {
                panic!("Expected all None");
            }
        }
    }
}
