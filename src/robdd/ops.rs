use super::parser::{ParseNode, GrammerItem};

pub fn replace_none_dont_care(values: &mut Vec<Option<bool>>, dont_care_value: bool) {
    for value in values {
        *value = match value {
            Some(_) => *value,
            None => Some(dont_care_value)
        }
    }
}

pub fn evaluate_expression(expr: &ParseNode, values: &Vec<Option<bool>>) -> Result<bool, String> {
    match expr.entry {
        GrammerItem::Not => evaluate_not(&expr.children, values),
        GrammerItem::And => evaluate_and(&expr.children, values),
        GrammerItem::Or => evaluate_or(&expr.children, values),
        GrammerItem::Imp => evaluate_imp(&expr.children, values),
        GrammerItem::Equiv => evaluate_equiv(&expr.children, values),
        GrammerItem::Var(x) => evaluate_var(x, values),
        GrammerItem::Literal(value) => Ok(value),
    }
}

pub fn evaluate_not(children: &Vec<ParseNode>, values: &Vec<Option<bool>>) -> Result<bool, String> {
    let lhs = evaluate_expression(&children[0], values)?;
    Ok(!lhs)
}

pub fn evaluate_and(children: &Vec<ParseNode>, values: &Vec<Option<bool>>) -> Result<bool, String> {
    let lhs = evaluate_expression(&children[0], values)?;
    let rhs = evaluate_expression(&children[1], values)?;
    Ok(lhs && rhs)
}

pub fn evaluate_or(children: &Vec<ParseNode>, values: &Vec<Option<bool>>) -> Result<bool, String> {
    let lhs = evaluate_expression(&children[0], values)?;
    let rhs = evaluate_expression(&children[1], values)?;
    Ok(lhs || rhs)
}

pub fn implication(values: (bool, bool)) -> bool {
    if let (true, false) = values {
        false
    } else {
        true
    }
}

pub fn evaluate_imp(children: &Vec<ParseNode>, values: &Vec<Option<bool>>) -> Result<bool, String> {
    let lhs = evaluate_expression(&children[0], values)?;
    let rhs = evaluate_expression(&children[1], values)?;
    Ok(implication((lhs, rhs)))
}

pub fn evaluate_equiv(children: &Vec<ParseNode>, values: &Vec<Option<bool>>) -> Result<bool, String> {
    let lhs = evaluate_expression(&children[0], values)?;
    let rhs = evaluate_expression(&children[1], values)?;
    Ok(lhs == rhs)
}

pub fn evaluate_var(var: u32, values: &Vec<Option<bool>>) -> Result<bool, String> {
    match values.get(var as usize) {
        Some(Some(value)) => Ok(*value),
        _ => Err(format!("Could not find value for variable x{}",var))
    }
}

#[cfg(test)]
mod tests {
    use crate::robdd::parser::parse;
    use super::*;

    // complex checks
    #[test]
    fn eval_simple_vars(){
        let test = "and(x1, x2)";
        let values = vec![Some(false), Some(true), Some(true)];
        let res = parse::parse(test)
            .and_then(|(t, _n)| evaluate_expression(&t, &values));
        assert_eq!(Ok(true), res);
        let values = vec![Some(false), Some(true), Some(false)];
        let res = parse::parse(test)
            .and_then(|(t, _n)| evaluate_expression(&t, &values));
        assert_eq!(Ok(false), res);
    }

    #[test]
    fn eval_value_not_present(){
        let test = "and(x1, x2)";
        let values = vec![Some(false), Some(true)];
        assert!(parse::parse(test)
                .and_then(|(t, _n)| evaluate_expression(&t, &values))
                .is_err());
    }

    #[test]
    fn eval_value_not_set() {
        let test = "and(x1, x2)";
        let values = vec![Some(false), Some(true), None];
        assert!(parse::parse(test)
                .and_then(|(t, _n)| evaluate_expression(&t, &values))
                .is_err());
    }

    #[test]
    fn eval_layers1() {
        let test = "or(x2, equiv(and(x3, 0), not(x1)))";
        // x3 doesnt matter
        // x2 || x1
        let values = vec![Some(false), Some(true), Some(false), Some(false)];
        let res = parse::parse(test)
            .and_then(|(t, _n)| evaluate_expression(&t, &values));
        assert_eq!(Ok(true), res);
        let values = vec![Some(false), Some(false), Some(false), Some(false)];
        let res = parse::parse(test)
            .and_then(|(t, _n)| evaluate_expression(&t, &values));
        assert_eq!(Ok(false), res);
        let values = vec![Some(false), Some(false), Some(false), Some(true)];
        let res = parse::parse(test)
            .and_then(|(t, _n)| evaluate_expression(&t, &values));
        assert_eq!(Ok(false), res);
        let values = vec![Some(false), Some(false), Some(true), Some(false)];
        let res = parse::parse(test)
            .and_then(|(t, _n)| evaluate_expression(&t, &values));
        assert_eq!(Ok(true), res);
    }

    // basic truth table checks
    #[test]
    fn eval_not_basic(){
        let test = "not(1)";
        let values = Vec::new();
        let res = parse::parse(test)
            .and_then(|(t, _n)| evaluate_expression(&t, &values));
        assert_eq!(Ok(false), res);
        let test = "not(not(1))";
        let res = parse::parse(test)
            .and_then(|(t, _n)| evaluate_expression(&t, &values));
        assert_eq!(Ok(true), res);
    }
    #[test]
    fn eval_and_basic(){
        let test = "and(0,0)";
        let values = Vec::new();
        let res = parse::parse(test)
            .and_then(|(t, _n)| evaluate_expression(&t, &values));
        assert_eq!(Ok(false), res);
        let test = "and(1,0)";
        let res = parse::parse(test)
            .and_then(|(t, _n)| evaluate_expression(&t, &values));
        assert_eq!(Ok(false), res);
        let test = "and(0,1)";
        let res = parse::parse(test)
            .and_then(|(t, _n)| evaluate_expression(&t, &values));
        assert_eq!(Ok(false), res);
        let test = "and(1,1)";
        let res = parse::parse(test)
            .and_then(|(t, _n)| evaluate_expression(&t, &values));
        assert_eq!(Ok(true), res);
    }
    #[test]
    fn eval_or_basic(){
        let test = "or(0,0)";
        let values = Vec::new();
        let res = parse::parse(test)
            .and_then(|(t, _n)| evaluate_expression(&t, &values));
        assert_eq!(Ok(false), res);
        let test = "or(1,0)";
        let res = parse::parse(test)
            .and_then(|(t, _n)| evaluate_expression(&t, &values));
        assert_eq!(Ok(true), res);
        let test = "or(0,1)";
        let res = parse::parse(test)
            .and_then(|(t, _n)| evaluate_expression(&t, &values));
        assert_eq!(Ok(true), res);
        let test = "or(1,1)";
        let res = parse::parse(test)
            .and_then(|(t, _n)| evaluate_expression(&t, &values));
        assert_eq!(Ok(true), res);
    }
    #[test]
    fn eval_imp_basic(){
        let test = "imp(0,0)";
        let values = Vec::new();
        let res = parse::parse(test)
            .and_then(|(t, _n)| evaluate_expression(&t, &values));
        assert_eq!(Ok(true), res);
        let test = "imp(1,0)";
        let res = parse::parse(test)
            .and_then(|(t, _n)| evaluate_expression(&t, &values));
        assert_eq!(Ok(false), res);
        let test = "imp(0,1)";
        let res = parse::parse(test)
            .and_then(|(t, _n)| evaluate_expression(&t, &values));
        assert_eq!(Ok(true), res);
        let test = "imp(1,1)";
        let res = parse::parse(test)
            .and_then(|(t, _n)| evaluate_expression(&t, &values));
        assert_eq!(Ok(true), res);
    }
    #[test]
    fn eval_equiv_basic(){
        let test = "equiv(0,0)";
        let values = Vec::new();
        let res = parse::parse(test)
            .and_then(|(t, _n)| evaluate_expression(&t, &values));
        assert_eq!(Ok(true), res);
        let test = "equiv(1,0)";
        let res = parse::parse(test)
            .and_then(|(t, _n)| evaluate_expression(&t, &values));
        assert_eq!(Ok(false), res);
        let test = "equiv(0,1)";
        let res = parse::parse(test)
            .and_then(|(t, _n)| evaluate_expression(&t, &values));
        assert_eq!(Ok(false), res);
        let test = "equiv(1,1)";
        let res = parse::parse(test)
            .and_then(|(t, _n)| evaluate_expression(&t, &values));
        assert_eq!(Ok(true), res);
    }
}
