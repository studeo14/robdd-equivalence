use super::LexItem;

use std::iter::Peekable;

// create a lex list from an input string
pub fn lex(expr: &str) -> Result<Vec<LexItem>, String> {
    let mut items: Vec<LexItem> = Vec::new();
    let mut it = expr.chars().peekable();

    while let Some(&c) = it.peek() {
        // need to match:
        // Op -- not, and, or, imp, equiv
        // Var -- x#
        // Literal -- 1, 0
        // Paren -- can prob ignore because there is no
        // order of ops in this grammar
        match c {
            'x' => {
                it.next(); // move to #
                let n = get_number(&mut it);
                items.push(LexItem::Var(n));
            },
            'n' | 'a' | 'o' | 'i' | 'e' => {
                let mut temp = String::new();
                while let Some(&new_c) = it.peek() {
                    match new_c {
                        '(' => {break;},
                        _ => {
                            temp.push(new_c);
                            it.next();
                        }
                    }
                }
                items.push(LexItem::Op(temp));
            },
            '(' | ')' => { // ignore for now
                it.next();
            },
            '0' => {
                it.next();
                items.push(LexItem::Literal(false));
            },
            '1' => {
                it.next();
                items.push(LexItem::Literal(true));
            },
            ' ' | ',' | '\n' | '\t' => { //ignore
                it.next();
            },
            _ => {
                return Err(format!("Unexpected character {}", c));
            }
        }
    }
    Ok(items)
}

// read the numbers after an x and convert it to an int
fn get_number<T>(iter: &mut Peekable<T>) -> u32
    where T: Iterator<Item = char>
{
    // buffer string
    let mut temp = String::new();
    // while avail and a number
    while let Some(&c) = iter.peek() {
        match c {
            '0'...'9' => {
                // add to buffer
                temp.push(c);
                iter.next();
            },
            // done
            _ => {
                break;
            }
        }
    }
    // try to convert
    let n: u32 = temp.parse().expect("Unable to parse number");
    n
}
