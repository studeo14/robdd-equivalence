use super::lex;
use super::{GrammerItem, LexItem, ParseNode};

use std::collections::HashSet;

pub fn to_infix(expr: &ParseNode) -> String {
    match expr.entry {
        GrammerItem::Not => format!("(~{})", to_infix(&expr.children[0])),
        GrammerItem::And => format!(
            "({} and {})",
            to_infix(&expr.children[0]),
            to_infix(&expr.children[1])
        ),
        GrammerItem::Or => format!(
            "({} or {})",
            to_infix(&expr.children[0]),
            to_infix(&expr.children[1])
        ),
        GrammerItem::Imp => format!(
            "({} => {})",
            to_infix(&expr.children[0]),
            to_infix(&expr.children[1])
        ),
        GrammerItem::Equiv => format!(
            "({} <=> {})",
            to_infix(&expr.children[0]),
            to_infix(&expr.children[1])
        ),
        GrammerItem::Var(var) => format!("x{}", var),
        GrammerItem::Literal(value) => format!("{}", if value { "T" } else { "F" }),
    }
}

// public facing parse function
// expr is the input string
// returns the parse tree Result and the number of variables
pub fn parse(expr: &str) -> Result<(ParseNode, u32), String> {
    // lex or err
    // try to lex the input
    // return the error if there is one
    // if no error then parse_from_lex
    match lex::lex(expr) {
        Ok(v) => parse_from_lex(&v),
        Err(e) => Err(e),
    }
}

pub fn parse_from_lex(items: &Vec<LexItem>) -> Result<(ParseNode, u32), String> {
    // parse the lex items recursively
    // if the index of the last item is not equal to the
    // total number of lex items
    // then return an error else return the ParseNode tree
    // println!("Len: {}", items.len());

    let mut varmap = HashSet::new();
    // parse staring at the first lex item
    parse_expr(&items, 0, &mut varmap)
        // if there is no error
        .and_then(|(p, i)| {
            if (i + 1) == items.len() {
                Ok((p, varmap.len() as u32))
            } else {
                Err(format!(
                    "Expected end of input, found {:?} at {}",
                    items[i], i
                ))
            }
        })
}

// recursive function for parsing the lex items
fn parse_expr(
    items: &Vec<LexItem>,
    pos: usize,
    varmap: &mut HashSet<u32>,
) -> Result<(ParseNode, usize), String> {
    // get current lex item
    let item = items.get(pos);

    // pattern match
    match item {
        // operator
        Some(LexItem::Op(name)) => {
            // create the node
            let mut node = ParseNode::new(match name.as_ref() {
                "not" => GrammerItem::Not,
                "and" => GrammerItem::And,
                "or" => GrammerItem::Or,
                "imp" => GrammerItem::Imp,
                "equiv" => GrammerItem::Equiv,
                _ => return Err(format!("Unknown operator {}", name)),
            });
            // recurse or return Err
            let (lhs, i) = parse_expr(items, pos + 1, varmap)?;
            node.children.push(lhs);
            // special case for not
            let next = if node.entry != GrammerItem::Not {
                // recurse or return Err
                let (rhs, temp) = parse_expr(items, i + 1, varmap)?;
                node.children.push(rhs);
                temp
            } else {
                i
            };
            // return node and the next lex item
            Ok((node, next))
        }
        Some(LexItem::Literal(value)) => {
            let node = ParseNode::new(GrammerItem::Literal(*value));
            Ok((node, pos))
        }
        Some(LexItem::Var(var)) => {
            let node = ParseNode::new(GrammerItem::Var(*var));
            varmap.insert(*var);
            Ok((node, pos))
        }
        _ => Err(format!("Unknown LexItem {:?}", item)),
    }
}
