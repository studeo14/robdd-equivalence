// definitions
#[derive(Debug, Clone, PartialEq)]
pub enum GrammerItem {
    Not,
    And,
    Or,
    Imp,
    Equiv,
    Var(u32),
    Literal(bool),
}

// parse tree
#[derive(Debug, Clone, PartialEq)]
pub struct ParseNode {
    pub children: Vec<ParseNode>,
    pub entry: GrammerItem,
}

#[derive(Debug, Clone, PartialEq)]
pub enum LexItem {
    Op(String),
    Literal(bool),
    Var(u32),
}

pub mod lex;
pub mod parse;

impl ParseNode {
    pub fn new(entry: GrammerItem) -> ParseNode {
        ParseNode {
            children: Vec::new(),
            entry,
        }
    }

    pub fn print(&self) {
        println!("{:?}", self.entry);
        for child in self.children.iter() {
            child.print_child(1);
        }
    }

    fn print_child(&self, level: usize) {
        print!("{}", "\t".repeat(level - 1));
        print!("|-------");
        println!("{:?}", self.entry);
        for child in self.children.iter() {
            child.print_child(level + 1);
        }
    }

    pub fn rebuild_expr(&self) {
        // print self
        match self.entry {
            GrammerItem::Not => {
                print!("not(");
                //print lhs
                self.children[0].rebuild_expr();
                //print end paren
                print!(")");
            }
            GrammerItem::And => {
                print!("and(");
                //print lhs
                self.children[0].rebuild_expr();
                //print delim
                print!(", ");
                //print rhs
                self.children[1].rebuild_expr();
                //print end paren
                print!(")");
            }
            GrammerItem::Or => {
                print!("or(");
                //print lhs
                self.children[0].rebuild_expr();
                //print delim
                print!(", ");
                //print rhs
                self.children[1].rebuild_expr();
                //print end paren
                print!(")");
            }
            GrammerItem::Imp => {
                print!("imp(");
                //print lhs
                self.children[0].rebuild_expr();
                //print delim
                print!(", ");
                //print rhs
                self.children[1].rebuild_expr();
                //print end paren
                print!(")");
            }
            GrammerItem::Equiv => {
                print!("equiv(");
                //print lhs
                self.children[0].rebuild_expr();
                //print delim
                print!(", ");
                //print rhs
                self.children[1].rebuild_expr();
                //print end paren
                print!(")");
            }
            GrammerItem::Var(num) => {
                print!("x{}", num);
            }
            GrammerItem::Literal(value) => {
                print!("{}", value as u32);
            }
        }
    }

    pub fn rebuild_expr_to_string(&self, output: &mut String) {
        // print self
        match self.entry {
            GrammerItem::Not => {
                output.push_str("not(");
                //print lhs
                self.children[0].rebuild_expr_to_string(output);
                //print end paren
                output.push_str(")");
            }
            GrammerItem::And => {
                output.push_str("and(");
                //print lhs
                self.children[0].rebuild_expr_to_string(output);
                //print delim
                output.push_str(", ");
                //print rhs
                self.children[1].rebuild_expr_to_string(output);
                //print end paren
                output.push_str(")");
            }
            GrammerItem::Or => {
                output.push_str("or(");
                //print lhs
                self.children[0].rebuild_expr_to_string(output);
                //print delim
                output.push_str(", ");
                //print rhs
                self.children[1].rebuild_expr_to_string(output);
                //print end paren
                output.push_str(")");
            }
            GrammerItem::Imp => {
                output.push_str("imp(");
                //print lhs
                self.children[0].rebuild_expr_to_string(output);
                //print delim
                output.push_str(", ");
                //print rhs
                self.children[1].rebuild_expr_to_string(output);
                //print end paren
                output.push_str(")");
            }
            GrammerItem::Equiv => {
                output.push_str("equiv(");
                //print lhs
                self.children[0].rebuild_expr_to_string(output);
                //print delim
                output.push_str(", ");
                //print rhs
                self.children[1].rebuild_expr_to_string(output);
                //print end paren
                output.push_str(")");
            }
            GrammerItem::Var(num) => {
                output.push_str(format!("x{}", num).as_ref());
            }
            GrammerItem::Literal(value) => {
                output.push_str(format!("{}", value as u32).as_ref());
            }
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn basic_check() {
        let test = "and(not(equiv(x1, x2)), or(x20, imp(x0, x1)))";
        let mut res = String::new();
        match super::parse::parse(test) {
            Ok((t, _n)) => t.rebuild_expr_to_string(&mut res),
            Err(e) => {
                println!("Error: {}", e);
                res.push_str("Error");
            }
        }
        assert_eq!(test, res);
        let test = "or(x0, and(equiv(imp(or(x1, 0), and(x2, x3)), x4), imp(imp(not(x5), 1), imp(and(x1, x1), not(1)))))";
        let mut res = String::new();
        match super::parse::parse(test) {
            Ok((t, _n)) => t.rebuild_expr_to_string(&mut res),
            Err(e) => {
                println!("Error: {}", e);
                res.push_str("Error");
            }
        }
        assert_eq!(test, res);
        let test = "or(0, x0)";
        let mut res = String::new();
        match super::parse::parse(test) {
            Ok((t, _n)) => t.rebuild_expr_to_string(&mut res),
            Err(e) => {
                println!("Error: {}", e);
                res.push_str("Error");
            }
        }
        assert_eq!(test, res);
        let test =
            "imp(not(equiv(and(equiv(x1, x2), imp(0, x3)), x1)), or(or(not(and(x4, x5)), x1), x6))";
        let mut res = String::new();
        match super::parse::parse(test) {
            Ok((t, _n)) => t.rebuild_expr_to_string(&mut res),
            Err(e) => {
                println!("Error: {}", e);
                res.push_str("Error");
            }
        }
        assert_eq!(test, res);
        let test = "or(x1, equiv(and(x2, 0), not(x0)))";
        let mut res = String::new();
        match super::parse::parse(test) {
            Ok((t, _n)) => t.rebuild_expr_to_string(&mut res),
            Err(e) => {
                println!("Error: {}", e);
                res.push_str("Error");
            }
        }
        assert_eq!(test, res);
        let test = "imp(equiv(or(and(equiv(0, 1), 1), 1), not(or(and(x1, x1), x1))), and(1, x0))";
        let mut res = String::new();
        match super::parse::parse(test) {
            Ok((t, _n)) => t.rebuild_expr_to_string(&mut res),
            Err(e) => {
                println!("Error: {}", e);
                res.push_str("Error");
            }
        }
        assert_eq!(test, res);
    }

    #[test]
    fn check_num_vars() {
        let test = "imp(equiv(or(and(equiv(0, 1), 1), 1), not(or(and(x1, x1), x1))), and(1, x0))";
        let res = match super::parse::parse(test) {
            Ok((_t, n)) => n,
            Err(_e) => 0,
        };
        assert_eq!(2, res);
        let test =
            "imp(not(equiv(and(equiv(x1, x2), imp(0, x3)), x1)), or(or(not(and(x4, x5)), x1), x6))";
        let res = match super::parse::parse(test) {
            Ok((_t, n)) => n,
            Err(_e) => 0,
        };
        assert_eq!(6, res);
        let test = "or(equiv(x1, x2), x3)";
        let res = match super::parse::parse(test) {
            Ok((_t, n)) => n,
            Err(_e) => 0,
        };
        assert_eq!(3, res);
    }
}
