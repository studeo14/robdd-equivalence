#!/usr/bin/python3

# Need to :
# Generate random, valid expression
# Consisting of:
# and, or, not, imp, equiv, var, literal

# first level must be an op
# each op must have at least one other op as an argument until max_rec_depth is reached
# Then just vars and literals

# choosing vars:
# 75% chance of new var, otherwise reuse number

# literals: 0,1 @ 50%
import random
import sys

class Generator():
    def __init__(self):
        self.oplist = ['and','or','not','imp','equiv']
        #self.endlist = ['var','literal']
        self.endlist = ['var']
        self.fulllist = self.oplist + self.endlist

        self.varlist = [True, True, True, False]

        self.existingvars = {1:True}
        self.nextvar = 1
        self.deepest = 0

    def do_op(self,op, position, max_depth):
        expr = ""
        expr += op + '(' + self.gen_subexpression(max_depth - 1)
        if op != 'not':
            expr += ', ' + self.gen_subexpression(max_depth - 1)
        expr += ')'
        if position == 0:
            expr += ', '
        return expr

    def do_var_literal(self,op, position):
        expr = ""
        if op == 'var':
            # new or used?
            new_used = random.choice(self.varlist)
            varnum = 1
            if new_used: #new
                self.existingvars[self.nextvar] = True
                varnum = self.nextvar
                self.nextvar += 1
                if self.nextvar > self.maxvar:
                    self.nextvar = self.maxvar
            else:
                varnum = random.choice(list(self.existingvars.keys()))
            expr += 'x{}'.format(varnum)
        else:
            literal = random.randint(0,1)
            expr += '{}'.format(literal)
        if position == 0:
            expr += ', '

        return expr

    def gen_subexpression(self,max_depth):
        # sub level
        expr = ""
        if (self.maxdepth - max_depth) > self.deepest:
            self.deepest = self.maxdepth - max_depth
        if max_depth != 0:
            op2 = random.choice(self.fulllist)
            if op2 in self.oplist: # if op
                expr += self.do_op(op2, 1, max_depth)
            else:             # var or literal
                expr += self.do_var_literal(op2, 1)
        else:
            # two var/literals
            op2 = random.choice(self.endlist)
            expr += self.do_var_literal(op2, 1)

        return expr

    def gen_expression(self, max_depth=5, max_var=10):
        # first level
        expr = ""
        op = random.choice(self.oplist)
        self.maxdepth = max_depth
        self.maxvar = max_var
        expr += self.do_op(op, 1, max_depth)
        return expr
    def get_max_var(self):
        return (list(self.existingvars.keys())[-1], self.deepest)

def main():
    if (len(sys.argv) > 1):
        max_var = int(sys.argv[1])
    else:
        max_var = 30
    for i in range(1):
        gen = Generator()
        MAX_DEPTH = 8
        expr = gen.gen_expression(8, max_var)
        temp = gen.get_max_var()
        print("Largest variable: %d, deepest: %d" % (temp), file=sys.stderr)
        print(expr)

if __name__ == '__main__':
    main()
